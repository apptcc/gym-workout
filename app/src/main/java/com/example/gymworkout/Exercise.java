package com.example.gymworkout;


public class Exercise {

    private String id;
    private String name;
    private String muscularGroup;
    private String serie;
    private String imageUrl;
    private String itemWorkoutId;

    public Exercise(String id, String name, String muscularGroup, String serie, String imageUrl) {
        this.id = id;
        this.name = name;
        this.muscularGroup = muscularGroup;
        this.serie = serie;
        this.imageUrl = imageUrl;
    }

    public Exercise(){};


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMuscularGroup() {
        return muscularGroup;
    }

    public void setMuscularGroup(String muscularGroup) {
        this.muscularGroup = muscularGroup;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getItemWorkoutId() {
        return itemWorkoutId;
    }

    public void setItemWorkoutId(String workoutId) {
        this.itemWorkoutId = workoutId;
    }
}