package com.example.gymworkout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private Context context;
    private List<Exercise> exercises;
    private Boolean exerciseNew;
    private String workoutId;

    public ImageAdapter(Context context, List<Exercise> exercises, Boolean exerciseNew, String workoutId){
        this.context = context;
        this.exercises = exercises;
        this.exerciseNew = exerciseNew;
        this.workoutId = workoutId;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_exercise, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        Exercise exercise = exercises.get(position);

        if(exerciseNew){
            holder.textViewName.setText(exercise.getName());
            holder.textViewGroup.setText(exercise.getMuscularGroup());
            holder.textViewSerie.setVisibility(View.INVISIBLE);
            holder.textViewSerieLabel.setVisibility(View.INVISIBLE);
            Picasso.with(context)
                .load(exercise.getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.imageView);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    LinearLayout layout = new LinearLayout(context);

                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(50, 10, 150, 0);

                    EditText editTextSeries = new EditText(context);
                    editTextSeries.setHint("Número de séries:");
                    editTextSeries.setInputType(InputType.TYPE_CLASS_NUMBER);
                    layout.addView(editTextSeries, params);

                    EditText editTextRepetitions = new EditText(context);
                    editTextRepetitions.setHint("Número de Repetições:");
                    editTextRepetitions.setInputType(InputType.TYPE_CLASS_NUMBER);
                    layout.addView(editTextRepetitions, params);

                    alertDialog.setView(layout);
                    alertDialog.setTitle("Adicionar execício '" + exercise.getName() + "' ao seu treino?");
                    alertDialog.setPositiveButton(Html.fromHtml("<font color='black'>Cadastrar</font>"), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    exercise.setSerie(editTextSeries.getText().toString().trim() + " x " + editTextRepetitions.getText().toString().trim());

                                    if (context instanceof WorkoutExercises) {
                                        ((WorkoutExercises)context).createWorkoutExercise(exercise);
                                    }
                                }
                            });
                    alertDialog.setNegativeButton(Html.fromHtml("<font color='black'>Cancelar</font>"), null);
                    alertDialog.setIcon(android.R.drawable.ic_input_add);
                    alertDialog.show();
                }
            });
        } else {
            holder.textViewName.setText(exercise.getName());
            holder.textViewGroup.setText(exercise.getMuscularGroup());
            holder.textViewSerie.setText(exercise.getSerie());
            Picasso.with(context)
                .load(exercise.getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.imageView);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    LinearLayout layout = new LinearLayout(context);

                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(50, 10, 150, 0);

                    EditText editTextSeries = new EditText(context);
                    editTextSeries.setHint("Número de séries:");
                    editTextSeries.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editTextSeries.setText(exercise.getSerie().split(" ")[0]);
                    layout.addView(editTextSeries, params);

                    EditText editTextRepetitions = new EditText(context);
                    editTextRepetitions.setHint("Número de Repetições:");
                    editTextRepetitions.setInputType(InputType.TYPE_CLASS_NUMBER);
                    editTextRepetitions.setText(exercise.getSerie().split(" ")[2]);
                    layout.addView(editTextRepetitions, params);

                    alertDialog.setView(layout);
                    alertDialog.setTitle("Editar execício '" + exercise.getName() + "'");
                    alertDialog.setPositiveButton(Html.fromHtml("<font color='black'>Atualizar</font>"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            exercise.setSerie(editTextSeries.getText().toString().trim() + " x " + editTextRepetitions.getText().toString().trim());
                            if (context instanceof WorkoutExercises) {
                                ((WorkoutExercises)context).updateWorkoutExercise(exercise);
                            }
                        }
                    });
                    alertDialog.setNegativeButton(Html.fromHtml("<font color='black'>Cancelar</font>"), null);
                    alertDialog.setNeutralButton(Html.fromHtml("<font color='red'>Deletar</font>"),  new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            exercise.setSerie(editTextSeries.getText().toString().trim() + " x " + editTextRepetitions.getText().toString().trim());

                            if (context instanceof WorkoutExercises) {
                                ((WorkoutExercises)context).deleteWorkoutExercise(exercise);
                            }
                        }
                    });
                    alertDialog.setIcon(android.R.drawable.ic_menu_edit);
                    alertDialog.show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return exercises.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{
        public TextView textViewName;
        public TextView textViewSerieLabel;
        public TextView textViewSerie;
        public TextView textViewGroup;
        public ImageView imageView;
        public CardView cardView;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textViewName);
            textViewGroup = itemView.findViewById(R.id.textViewGroup);
            textViewSerie = itemView.findViewById(R.id.textViewSerie);
            textViewSerieLabel = itemView.findViewById(R.id.textViewSerieLabel);
            imageView = itemView.findViewById(R.id.imageView);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

}
