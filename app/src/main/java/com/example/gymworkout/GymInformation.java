package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class GymInformation extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private String userType;

    private TextView textViewCadastrar;
    private TextView textViewLabel;
    private EditText editTextNameGymActivity;
    private EditText editTextHour;
    private EditText editTextLocal;
    private CheckBox checkboxSeg, checkboxTer, checkboxQua, checkboxQui, checkboxSex, checkboxSab, checkboxDom;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private CustomAdapterGymInformation customAdapter;
    private BottomNavigationView navigationView;
    private BottomNavigationView navigationViewInstructor;
    private BottomNavigationView navigationViewGym;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<GymActivity> gymActivityList;

    private LinearLayout noData;
    private ImageView noDataImage;
    private TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_information);

        getSupportActionBar().setTitle("Atividades da academia");


        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationViewInstructor = (BottomNavigationView) findViewById(R.id.navigationViewInstructor);
        navigationViewGym = (BottomNavigationView) findViewById(R.id.navigationViewGym);

        floatingActionButton = findViewById(R.id.floatingActionButton);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        noData = findViewById(R.id.noData);
        noDataImage = findViewById(R.id.noDataImage);
        noDataText = findViewById(R.id.noDataText);

        recyclerView = findViewById(R.id.recyclerView);

        menuNavigation();
        menuNavigationInstructor();
        menuNavigationGym();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDialogTrainingRegistration(false, null);
            }
        });
    }

    @Override
    public void onRefresh() {

    }

    @Override
    protected void onStart() {
        super.onStart();

        userType = getIntent().getStringExtra("userType");

        setViewUserType();

        getGymInformationUserType();
    }

    private void setViewUserType(){
        if(userType.equals("Aluno")) {
            floatingActionButton.setVisibility(View.INVISIBLE);
            navigationView.setSelectedItemId(R.id.navigationGym);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
            navigationViewGym.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Professor")){
            floatingActionButton.setVisibility(View.INVISIBLE);
            navigationViewInstructor.setSelectedItemId(R.id.navigationGym);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewGym.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Academia")){
            navigationViewGym.setSelectedItemId(R.id.navigationGym);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
        }
    }

    public void menuNavigation(){
        navigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(GymInformation.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(GymInformation.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(GymInformation.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationInstructor(){
        navigationViewInstructor.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(GymInformation.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(GymInformation.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(GymInformation.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(GymInformation.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationGym(){
        navigationViewGym.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(GymInformation.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(GymInformation.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public  void displayDialogTrainingRegistration(Boolean update, GymActivity gymActivity) {

        Dialog dialog = new Dialog(GymInformation.this);
        dialog.setContentView(R.layout.layout_gym_activity_registration);
        setDialogComponents(dialog);

        if(update){
            textViewLabel.setText("Atualizar atividade da academia");
            textViewCadastrar.setText("Atualizar");
            editTextNameGymActivity.setText(gymActivity.getName());
            checkboxSeg.setChecked(gymActivity.getDaysWeek().contains("Seg") ? true : false);
            checkboxTer.setChecked(gymActivity.getDaysWeek().contains("Ter") ? true : false);
            checkboxQua.setChecked(gymActivity.getDaysWeek().contains("Qua") ? true : false);
            checkboxQui.setChecked(gymActivity.getDaysWeek().contains("Qui") ? true : false);
            checkboxSex.setChecked(gymActivity.getDaysWeek().contains("Sex") ? true : false);
            checkboxSab.setChecked(gymActivity.getDaysWeek().contains("Sab") ? true : false);
            checkboxDom.setChecked(gymActivity.getDaysWeek().contains("Dom") ? true : false);
            editTextHour.setText(gymActivity.getHour());
            editTextLocal.setText(gymActivity.getLocal());

            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateGymInformationUser(gymActivity);
                    dialog.dismiss();
                }
            });


        } else {
            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createGymInformationUser();
                    dialog.dismiss();
                }
            });
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setDialogComponents(Dialog dialog){
        editTextNameGymActivity = dialog.findViewById(R.id.editTextNameGymActivity);
        checkboxSeg = dialog.findViewById(R.id.checkboxSeg);
        checkboxTer = dialog.findViewById(R.id.checkboxTer);
        checkboxQua = dialog.findViewById(R.id.checkboxQua);
        checkboxQui = dialog.findViewById(R.id.checkboxQui);
        checkboxSex = dialog.findViewById(R.id.checkboxSex);
        checkboxSab = dialog.findViewById(R.id.checkboxSab);
        checkboxDom = dialog.findViewById(R.id.checkboxDom);
        editTextHour = dialog.findViewById(R.id.editTextHour);
        editTextLocal = dialog.findViewById(R.id.editTextLocal);
        textViewLabel = dialog.findViewById(R.id.textViewLabel);
        textViewCadastrar = dialog.findViewById(R.id.textViewCadastrar);
    }

    public void createGymInformationUser(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        GymActivity gymActivity = new GymActivity(
                editTextNameGymActivity.getText().toString(),
                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                createArrayWeekDaysTraining(),
                editTextHour.getText().toString(),
                editTextLocal.getText().toString()
        );

        db.collection("GymInformation")
        .add(gymActivity)
        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(GymInformation.this, "Atividade cadastrada com sucesso", Toast.LENGTH_SHORT).show();
            }
        });

        getGymInformationUserType();
    }

    private String createArrayWeekDaysTraining(){
        String daysWeek = "";

        if (checkboxSeg.isChecked()) {
            daysWeek = "Seg";
        }
        if (checkboxTer.isChecked()) {
            daysWeek += " Ter";
        }
        if (checkboxQua.isChecked()) {
            daysWeek += " Qua";
        }
        if (checkboxQui.isChecked()) {
            daysWeek += " Qui";
        }
        if (checkboxSex.isChecked()) {
            daysWeek += " Sex";
        }
        if (checkboxSab.isChecked()) {
            daysWeek += " Sab";
        }
        if (checkboxDom.isChecked()) {
            daysWeek += " Dom";
        }

        return daysWeek;
    }

    public void getGymInformationUserType(){
        if(userType.equals("Academia")) {
            getGymInformation(FirebaseAuth.getInstance().getCurrentUser().getUid());
        } else {
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            db.collection("MemberGym")
            .whereEqualTo ("userId", FirebaseAuth.getInstance().getCurrentUser().getUid())
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        if(task.getResult().size() > 0) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                getGymInformation(document.getString("gymId"));
                            }
                            noData.setVisibility(View.GONE);
                        } else {
                            noData.setVisibility(View.VISIBLE);
                            noDataText.setText("Você não está cadastrado em nenhuma academia!");
                        }
                    } else {
                    }
                }
            });
        }
    }

    public void getGymInformation(String gymId){
        gymActivityList = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("GymInformation")
        .whereEqualTo ("userGymId", gymId)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        GymActivity gymActivity = new GymActivity();
                        gymActivity.setId(document.getId());
                        gymActivity.setName(document.getString("name"));
                        gymActivity.setDaysWeek(document.getString("daysWeek"));
                        gymActivity.setHour(document.getString("hour"));
                        gymActivity.setLocal(document.getString("local"));
                        gymActivity.setUserGymId(document.getString("userGymId"));

                        gymActivityList.add(gymActivity);
                    }

                    customAdapter = new CustomAdapterGymInformation(GymInformation.this, gymActivityList, userType);
                    recyclerView.setAdapter(customAdapter);

                    if(gymActivityList.size() > 0){
                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                        if(!userType.equals("Academia")){
                            noDataText.setText("Sua academia não tem nenhuma atividade cadastrada.");
                        }
                    }
                } else {
                }
            }
        });

    }

    public void updateGymInformationUser(GymActivity gymActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("GymInformation")
        .document(gymActivity.getId())
        .update( "name", editTextNameGymActivity.getText().toString(),
                "daysWeek", createArrayWeekDaysTraining(),
                "hour", editTextHour.getText().toString(),
                "local", editTextLocal.getText().toString()
        )
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(GymInformation.this, "Atividade atualizada com sucesso", Toast.LENGTH_SHORT).show();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getGymInformationUserType();
    }

    public void deleteGymInformationUser(GymActivity gymActivity){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("GymInformation")
        .document(gymActivity.getId())
        .delete()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(GymInformation.this, "Atividade deletada com sucesso", Toast.LENGTH_SHORT).show();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getGymInformationUserType();
    }
}