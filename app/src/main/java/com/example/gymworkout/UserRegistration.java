package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class UserRegistration extends AppCompatActivity {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextName;
    private EditText editTextDocument;
    private EditText editTextAddress;
    private EditText editTextPhone;
    private Button buttonRegister;
    private String userId;
    private String userType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration); getSupportActionBar().hide(); // Esconder ActionBar

        setComponents();

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = editTextEmail.getText().toString();
                String password = editTextPassword.getText().toString();
                String name = editTextName.getText().toString();
                String document = editTextDocument.getText().toString();
                String address = editTextAddress.getText().toString();
                String phone = editTextPhone.getText().toString();

                if(name.isEmpty() || email.isEmpty() || password.isEmpty()
                        || document.isEmpty() || address.isEmpty() || phone.isEmpty() || userType.isEmpty()){
                    Toast.makeText(UserRegistration.this, "Preencha todos os dados!", Toast.LENGTH_SHORT).show();
                } else {
                    validateUser();
                }
            }
        });
    }

    private void setComponents(){
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextName = findViewById(R.id.editTextName);
        editTextDocument = findViewById(R.id.editTextDocument);
        editTextAddress = findViewById(R.id.editTextAddress);
        editTextPhone = findViewById(R.id.editTextPhone);
        buttonRegister = findViewById(R.id.buttonRegister);
    }

    private void validateUser(){

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(editTextEmail.getText().toString(), editTextPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    createUser();

                    Toast.makeText(UserRegistration.this, "Cadastro feito com sucesso!", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(UserRegistration.this, MainActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    String error;

                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e){
                        error = "Senha deve possuir no minímo 6 digítos.";
                    } catch (FirebaseAuthUserCollisionException e){
                        error = "Este usúario já existe!";
                    } catch (FirebaseAuthInvalidCredentialsException e){
                        error = "E-mail inválido.";
                    } catch (Exception e){
                        error = "Erro ao cadastrar usuário.";
                    }

                    Toast.makeText(UserRegistration.this, error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radioButtonAcademia:
                if (checked)
                    userType = "Academia";
                    break;
            case R.id.radioButtonAluno:
                if (checked)
                    userType = "Aluno";
                    break;
            case R.id.radioButtonProfessor:
                if (checked)
                    userType = "Professor";
                    break;
        }
    }

    private void createUser(){

        User user = new User(
                userType,
                editTextEmail.getText().toString(),
                editTextName.getText().toString(),
                editTextDocument.getText().toString(),
                editTextAddress.getText().toString(),
                editTextPhone.getText().toString()
        );

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        user.setId(userId);

        DocumentReference documentReference = db.collection("Users").document(userId);

        documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {



            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {


            }
        });
    }
}