package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WorkoutExercises extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private FirebaseFirestore db;

    private String workoutId;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private List<Exercise> exercises;
    private SwipeRefreshLayout swipeRefreshLayout;

    private RecyclerView recyclerViewExercisesNew;
    private Spinner spinner;

    private List<Exercise> exercisesNew;
    private ImageAdapter imageAdapter;

    private LinearLayout noData;
    private ImageView noDataImage;
    private TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_exercises);

        getSupportActionBar().setTitle(getIntent().getStringExtra("name") + " - Exercicíos");

        db = FirebaseFirestore.getInstance();
        workoutId = getIntent().getStringExtra("id");
        floatingActionButton = findViewById(R.id.floatingActionButton);
        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        noData = findViewById(R.id.noData);
        noDataImage = findViewById(R.id.noDataImage);
        noDataText = findViewById(R.id.noDataText);

        getExercises();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayBottomSheetExerciseRegistration();
            }
        });

//        Toast.makeText(this, getIntent().getStringExtra("id"), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
         getExercises();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void displayBottomSheetExerciseRegistration(){
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this,  R.style.CustomBottomSheetDialogTheme);

        View bottomSheetView = LayoutInflater.from(getApplicationContext())
                .inflate(
                        R.layout.layout_exercise_registration,
                        (LinearLayout)findViewById(R.id.cadastroTreino)
                );
//        BottomSheetBehavior.from(bottomSheetView).setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetDialog.setContentView(bottomSheetView);

//        spinner = (Spinner) bottomSheetView.findViewById(R.id.spinner);
        recyclerViewExercisesNew = (RecyclerView) bottomSheetView.findViewById(R.id.recyclerView);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.grupo_muscular, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//        spinner.setOnItemSelectedListener(this);


        getExercisesNew();
        bottomSheetDialog.show();
        getExercises();
    }

    public void getExercises(){
        exercises = new ArrayList<>();
        Set<String> exercisesId = new HashSet<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new GridLayoutManager(this, 2));

        db.collection("ItemWorkout")
        .whereEqualTo ("workoutId", workoutId)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Exercise exercise = new Exercise();
                        exercise.setId(document.getString("exerciseId"));
                        exercise.setSerie(document.getString("serie"));
                        exercise.setImageUrl(document.getString("imageUrl"));
                        exercise.setName(document.getString("name"));
                        exercise.setMuscularGroup(document.getString("muscularGroup"));
                        exercise.setItemWorkoutId(document.getId());

                        exercises.add(exercise);
                    }

                    imageAdapter = new ImageAdapter(WorkoutExercises.this, exercises, false, workoutId);
                    recyclerView.setAdapter(imageAdapter);

                    if(exercises.size() > 0){
                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }
                } else {
                }
            }
        });
    }

    private void getExercisesNew(){
        exercisesNew = new ArrayList<>();

        recyclerViewExercisesNew.setHasFixedSize(true);
        recyclerViewExercisesNew.setLayoutManager( new GridLayoutManager(this, 2));

        db.collection("Exercicios")
        .whereNotEqualTo ("name", null)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Exercise exercise = new Exercise();
                        exercise.setId(document.getId());
                        exercise.setName(document.getString("name"));
                        exercise.setImageUrl(document.getString("imageUrl"));
                        exercise.setMuscularGroup(document.getString("muscularGroup"));
                        exercisesNew.add(exercise);
                    }

                    imageAdapter = new ImageAdapter(WorkoutExercises.this, exercisesNew, true, workoutId);
                    recyclerViewExercisesNew.setAdapter(imageAdapter);
                }
            }
        });
    }

    public void createWorkoutExercise(Exercise exercise){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, String> itemWorkout = new HashMap<>();
        itemWorkout.put("workoutId", workoutId);
        itemWorkout.put("exerciseId", exercise.getId());
        itemWorkout.put("name", exercise.getName());
        itemWorkout.put("muscularGroup", exercise.getMuscularGroup());
        itemWorkout.put("serie", exercise.getSerie());
        itemWorkout.put("imageUrl", exercise.getImageUrl());

        db.collection("ItemWorkout")
                .add(itemWorkout)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {

                        db.collection("Workouts").document(workoutId).update( "exercisesNumber", FieldValue.increment(1))
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        getExercises();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                    }
                                });
                    }
                });
    }

    public void deleteWorkoutExercise(Exercise exercise){

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("ItemWorkout").document(exercise.getItemWorkoutId())
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        db.collection("Workouts").document(workoutId).update( "exercisesNumber", FieldValue.increment(-1))
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void unused) {
                                getExercises();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    public void updateWorkoutExercise(Exercise exercise){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("ItemWorkout")
        .document(exercise.getItemWorkoutId())
        .update( "serie", exercise.getSerie())
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(WorkoutExercises.this, "Exercício atualizado com sucesso", Toast.LENGTH_SHORT).show();
                getExercises();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

    }
}