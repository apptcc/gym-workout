package com.example.gymworkout;

public class MemberGym {

    private String id;
    private String gymId;
    private String userId;
    private String userName;
    private String userType;
    private String dateCreation;

    public MemberGym(){};

    public MemberGym(String gymId, String userId, String userName, String userType, String dateCreation) {
        this.gymId = gymId;
        this.userId = userId;
        this.userName = userName;
        this.userType = userType;
        this.dateCreation = dateCreation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGymId() {
        return gymId;
    }

    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }
}
