package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Workout extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private String userType;

    private TextView textViewCadastrar;
    private TextView textViewLabel;
    private EditText editTextNameTraining;
    private CheckBox checkboxSeg, checkboxTer, checkboxQua, checkboxQui, checkboxSex, checkboxSab, checkboxDom;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private CustomAdapterWorkouts customAdapter;
    private List<Training> workouts;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BottomNavigationView navigationView;
    private BottomNavigationView navigationViewInstructor;
    private BottomNavigationView navigationViewStudentMode;
    private LinearLayout noData;
    private ImageView noDataImage;
    private TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        getSupportActionBar().setTitle("Lista de treinos");

        floatingActionButton = findViewById(R.id.floatingActionButton);
        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationViewInstructor = (BottomNavigationView) findViewById(R.id.navigationViewInstructor);
        navigationViewStudentMode = (BottomNavigationView) findViewById(R.id.navigationViewStudentMode);

        noData = findViewById(R.id.noData);
        noDataImage = findViewById(R.id.noDataImage);
        noDataText = findViewById(R.id.noDataText);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        user.getUid();

        menuNavigation();
        menuNavigationInstructor();
        menuNavigationStudentMode();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDialogTrainingRegistration(false, null);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        userType = getIntent().getStringExtra("userType");

        setViewUserType();

        getWorkoutsUserType();
    }

    private void setViewUserType(){
        if(userType.equals("Aluno")) {
            navigationViewInstructor.setVisibility(View.INVISIBLE);
            navigationViewStudentMode.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Professor")){
            navigationViewInstructor.setSelectedItemId(R.id.navigationWorkout);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewStudentMode.setVisibility(View.INVISIBLE);
        }  else if(userType.equals("StudentMode")){
            getSupportActionBar().setTitle("Lista de treinos de " + getIntent().getStringExtra("userName"));
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        getWorkoutsUserType();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void menuNavigation(){
        navigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(Workout.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(Workout.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(Workout.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationInstructor(){
        navigationViewInstructor.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(Workout.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(Workout.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(Workout.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(Workout.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationStudentMode(){
        navigationViewStudentMode.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(Workout.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        intent.putExtra("instructorName", getIntent().getStringExtra("instructorName"));
                        intent.putExtra("userId", getIntent().getStringExtra("userId"));
                        intent.putExtra("userName", getIntent().getStringExtra("userName"));
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationExit: {
                        Intent intent = new Intent(Workout.this, UserInformation.class);
                        intent.putExtra("userType", "Professor");
                        startActivity(intent);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void getWorkoutsUserType(){
        if(userType.equals("StudentMode")){
            getWorkouts(getIntent().getStringExtra("userId"));
        } else {
            getWorkouts(FirebaseAuth.getInstance().getCurrentUser().getUid());
        }
    }

    public void getWorkouts(String userId){
        workouts = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Workouts")
        .whereEqualTo ("userId", userId)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Training training = new Training();
                        training.setId(document.getId());
                        training.setName(document.getString("name"));
                        training.setInstructorId(document.getString("instructorId"));
                        training.setInstructorName(document.getString("instructorName"));
                        training.setUserId(document.getString("userId"));
                        training.setExercisesNumber(document.getLong("exercisesNumber").intValue());
                        training.setDaysWeek(document.getString("daysWeek"));
                        training.setDateCreation(document.getString("dateCreation"));

                        workouts.add(training);
                    }

                    customAdapter = new CustomAdapterWorkouts(Workout.this, workouts, userType);
                    recyclerView.setAdapter(customAdapter);

                    if(workouts.size() > 0){
                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }
                } else {
                }
            }
        });

    }

    public  void displayDialogTrainingRegistration(Boolean update, Training training) {

        Dialog dialog = new Dialog(Workout.this);
        dialog.setContentView(R.layout.layout_training_registration);
        setDialogComponents(dialog);

        if(update){
            textViewLabel.setText("Atualizar Treino");
            textViewCadastrar.setText("Atualizar");
            editTextNameTraining.setText(training.getName());
            checkboxSeg.setChecked(training.getDaysWeek().contains("Seg") ? true : false);
            checkboxTer.setChecked(training.getDaysWeek().contains("Ter") ? true : false);
            checkboxQua.setChecked(training.getDaysWeek().contains("Qua") ? true : false);
            checkboxQui.setChecked(training.getDaysWeek().contains("Qui") ? true : false);
            checkboxSex.setChecked(training.getDaysWeek().contains("Sex") ? true : false);
            checkboxSab.setChecked(training.getDaysWeek().contains("Sab") ? true : false);
            checkboxDom.setChecked(training.getDaysWeek().contains("Dom") ? true : false);

            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(validateTraining(view)) {
                        updateTrainingUser(training);
                        dialog.dismiss();
                    }
                }
            });


        } else {
            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(validateTraining(view)) {
                        createTrainingUser();
                        dialog.dismiss();
                    }
                }
            });
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setDialogComponents(Dialog dialog){
        editTextNameTraining = dialog.findViewById(R.id.editTextNameTraining);
        checkboxSeg = dialog.findViewById(R.id.checkboxSeg);
        checkboxTer = dialog.findViewById(R.id.checkboxTer);
        checkboxQua = dialog.findViewById(R.id.checkboxQua);
        checkboxQui = dialog.findViewById(R.id.checkboxQui);
        checkboxSex = dialog.findViewById(R.id.checkboxSex);
        checkboxSab = dialog.findViewById(R.id.checkboxSab);
        checkboxDom = dialog.findViewById(R.id.checkboxDom);
        textViewLabel = dialog.findViewById(R.id.textViewLabel);
        textViewCadastrar = dialog.findViewById(R.id.textViewCadastrar);
    }

    public void createTrainingUser(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Training training = new Training();

        if(userType.equals("StudentMode")){
            training = new Training(
                    editTextNameTraining.getText().toString(),
                    getIntent().getStringExtra("userId"),
                    FirebaseAuth.getInstance().getCurrentUser().getUid(),
                    getIntent().getStringExtra("instructorName"),
                    0,
                    createArrayWeekDaysTraining(),
                    simpleDateFormat.format(Calendar.getInstance().getTime())
            );
        } else {
            training = new Training(
                    editTextNameTraining.getText().toString(),
                    FirebaseAuth.getInstance().getCurrentUser().getUid(),
                    "",
                    "Treino não cadastrado por um instrutor",
                    0,
                    createArrayWeekDaysTraining(),
                    simpleDateFormat.format(Calendar.getInstance().getTime())
            );
        }

        db.collection("Workouts")
        .add(training)
        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(Workout.this, "Treino cadastrado com sucesso", Toast.LENGTH_SHORT).show();
            }
        });

        getWorkoutsUserType();
    }

    public void updateTrainingUser(Training training){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Workouts")
        .document(training.getId())
        .update( "name", editTextNameTraining.getText().toString(), "daysWeek", createArrayWeekDaysTraining())
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(Workout.this, "Treino atualizado com sucesso", Toast.LENGTH_SHORT).show();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getWorkoutsUserType();
    }

    public void deleteTrainingUser(Training training){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Workouts").document(training.getId())
        .delete()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                db.collection("ItemWorkout")
                .whereEqualTo ("workoutId", training.getId())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                document.getReference().delete();
                            }

                            Toast.makeText(Workout.this, "Treino excluído com sucesso", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Workout.this, "Erro ao excluír treino, tente novamente mais tarde.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getWorkoutsUserType();
    }

    private Boolean validateTraining(View view){
        if(editTextNameTraining.getText().toString().isEmpty() || createArrayWeekDaysTraining().isEmpty()){
            Toast.makeText(view.getContext(), "Preencha nome e dias do treino!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private String createArrayWeekDaysTraining(){
        String daysWeek = "";

        if (checkboxSeg.isChecked()) {
            daysWeek = "Seg";
        }
        if (checkboxTer.isChecked()) {
            daysWeek += " Ter";
        }
        if (checkboxQua.isChecked()) {
            daysWeek += " Qua";
        }
        if (checkboxQui.isChecked()) {
            daysWeek += " Qui";
        }
        if (checkboxSex.isChecked()) {
            daysWeek += " Sex";
        }
        if (checkboxSab.isChecked()) {
            daysWeek += " Sab";
        }
        if (checkboxDom.isChecked()) {
            daysWeek += " Dom";
        }

        return daysWeek;
    }
}