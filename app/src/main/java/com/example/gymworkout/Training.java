package com.example.gymworkout;

import java.util.List;

public class Training {

    private String id;
    private String name;
    private String userId;
    private String instructorId;
    private String instructorName;
    private Integer exercisesNumber;
    private String daysWeek;
    private String dateCreation;

    public Training(){}

    public Training(String name, String userId, String instructorId, String instructorName, Integer exercisesNumber, String daysWeek, String dateCreation) {
        this.name = name;
        this.userId = userId;
        this.instructorId = instructorId;
        this.instructorName = instructorName;
        this.exercisesNumber = exercisesNumber;
        this.daysWeek = daysWeek;
        this.dateCreation = dateCreation;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructorId() {
        return instructorId;
    }

    public void setInstructorId(String instructorId) {
        this.instructorId = instructorId;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getExercisesNumber() {
        return exercisesNumber;
    }

    public void setExercisesNumber(Integer exercisesNumber) {
        this.exercisesNumber = exercisesNumber;
    }

    public String getDaysWeek() {
        return daysWeek;
    }

    public void setDaysWeek(String daysWeek) {
        this.daysWeek = daysWeek;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }
}
