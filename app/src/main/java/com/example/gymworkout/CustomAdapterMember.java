package com.example.gymworkout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapterMember extends RecyclerView.Adapter<CustomAdapterMember.CustomAdapterViewHolder> {

    private Context context;
    private List<MemberGym> memberGymList;
    private String userType;

    public CustomAdapterMember(Context context, List<MemberGym> memberGymList, String userType){
        this.context = context;
        this.memberGymList = memberGymList;
        this.userType = userType;
    }

    @NonNull
    @Override
    public CustomAdapterMember.CustomAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_member_gym, parent, false);
        return new CustomAdapterMember.CustomAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterMember.CustomAdapterViewHolder holder, int position) {

        MemberGym memberGym = memberGymList.get(position);

        holder.textViewNameMember.setText(memberGym.getUserName());
        holder.textViewMemberType.setText(memberGym.getUserType());
        holder.textViewCreationDate.setText(memberGym.getDateCreation());

        if(userType.equals("Professor")) {
            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (context instanceof UserInformation) {
                        ((UserInformation) context).profileMemberGym(memberGym);
                    }
                }
            });
        }

        holder.imageButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

                alertDialog.setTitle("Confirma a exclusão do membro " + memberGym.getUserName() + "?");
                alertDialog.setPositiveButton(Html.fromHtml("<font color='black'>Ok</font>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (context instanceof UserInformation) {
                            ((UserInformation)context).deleteMemberGym(memberGym);
                        }
                    }
                });
                alertDialog.setNegativeButton(Html.fromHtml("<font color='black'>Cancelar</font>"), null);
                alertDialog.setIcon(android.R.drawable.ic_input_add);
                alertDialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return memberGymList.size();
    }

    public class CustomAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewNameMember;
        public TextView textViewMemberType;
        public TextView textViewCreationDate;
        public ImageButton imageButtonDelete;
        public CardView cardView;

        public CustomAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewNameMember = itemView.findViewById(R.id.textViewNameMember);
            textViewMemberType = itemView.findViewById(R.id.textViewMemberType);
            textViewCreationDate = itemView.findViewById(R.id.textViewCreationDate);
            imageButtonDelete = itemView.findViewById(R.id.imageButtonDelete);
            cardView = itemView.findViewById(R.id.cardView);

            if(!userType.equals("Academia")){
                imageButtonDelete.setVisibility(View.INVISIBLE);
            }
        }
    }
}
