package com.example.gymworkout;

public class GymActivity {

    private String id;
    private String name;
    private String userGymId;
    private String daysWeek;
    private String hour;
    private String local;

    public GymActivity(){};

    public GymActivity(String name, String userGymId, String daysWeek, String hour, String local) {
        this.name = name;
        this.userGymId = userGymId;
        this.daysWeek = daysWeek;
        this.hour = hour;
        this.local = local;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserGymId() {
        return userGymId;
    }

    public void setUserGymId(String userGymId) {
        this.userGymId = userGymId;
    }

    public String getDaysWeek() {
        return daysWeek;
    }

    public void setDaysWeek(String daysWeek) {
        this.daysWeek = daysWeek;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }
}
