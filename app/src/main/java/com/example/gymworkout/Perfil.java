package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class Perfil extends AppCompatActivity {

    private String userType;

    private TextView textViewUserEmail;
    private TextView textViewUserName;
    private Button buttonLogout;

    private BottomNavigationView navigationView;
    private BottomNavigationView navigationViewInstructor;
    private BottomNavigationView navigationViewGym;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        getSupportActionBar().setTitle("Perfil");

        setComponents();

        menuNavigation();
        menuNavigationInstructor();
        menuNavigationGym();

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(Perfil.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        DocumentReference documentReference = db.collection("Users").document(userId);

        documentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException error) {
                if(documentSnapshot != null){
                    textViewUserName.setText(documentSnapshot.getString("name"));
                    textViewUserEmail.setText(email);
                }
            }
        });

        userType = getIntent().getStringExtra("userType");

        setViewUserType();
    }

    private void setComponents(){
        textViewUserEmail = findViewById(R.id.textViewUserEmail);
        textViewUserName = findViewById(R.id.textViewUserName);
        buttonLogout = findViewById(R.id.buttonLogout);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationViewInstructor = (BottomNavigationView) findViewById(R.id.navigationViewInstructor);
        navigationViewGym = (BottomNavigationView) findViewById(R.id.navigationViewGym);
    }


    private void setViewUserType(){
        if(userType.equals("Aluno")) {
            navigationView.setSelectedItemId(R.id.navigationProfile);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
            navigationViewGym.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Professor")){
            navigationViewInstructor.setSelectedItemId(R.id.navigationProfile);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewGym.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Academia")){
            navigationViewGym.setSelectedItemId(R.id.navigationProfile);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
        }
    }

    public void menuNavigation(){
        navigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(Perfil.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(Perfil.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(Perfil.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationInstructor(){
        navigationViewInstructor.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(Perfil.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(Perfil.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(Perfil.this, BodyInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(Perfil.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationGym(){
        navigationViewGym.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(Perfil.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(Perfil.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        return true;
                    }
                }

                return false;
            }
        });
    }
}