package com.example.gymworkout;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapterGymInformation extends RecyclerView.Adapter<CustomAdapterGymInformation.CustomAdapterViewHolder>{

    private Context context;
    private List<GymActivity> gymActivityList;
    private String userType;

    public CustomAdapterGymInformation(Context context, List<GymActivity> gymActivityList, String userType){
        this.context = context;
        this.gymActivityList = gymActivityList;
        this.userType = userType;
    }

    @NonNull
    @Override
    public CustomAdapterGymInformation.CustomAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_gym_activity, parent, false);
        return new CustomAdapterGymInformation.CustomAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterGymInformation.CustomAdapterViewHolder holder, int position) {

        GymActivity gymActivity = gymActivityList.get(position);

        holder.textViewNameGymActivity.setText(gymActivity.getName());
        holder.textViewDaysWeek.setText(gymActivity.getDaysWeek());
        holder.textViewHour.setText(gymActivity.getHour());
        holder.textViewLocal.setText(gymActivity.getLocal());

        holder.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete: {
                        if (context instanceof GymInformation) {
                            ((GymInformation)context).deleteGymInformationUser(gymActivity);
                        }
                        break;
                    }
                    case R.id.update: {
                        if (context instanceof GymInformation) {
                            ((GymInformation)context).displayDialogTrainingRegistration(true, gymActivity);
                        }
                        break;
                    }
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return gymActivityList.size();
    }

    public class CustomAdapterViewHolder  extends RecyclerView.ViewHolder {
        public TextView textViewNameGymActivity;
        public TextView textViewDaysWeek;
        public TextView textViewHour;
        public TextView textViewLocal;
        public CardView cardView;
        public androidx.appcompat.widget.Toolbar toolbar;

        public CustomAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewNameGymActivity = itemView.findViewById(R.id.textViewNameGymActivity);
            textViewDaysWeek = itemView.findViewById(R.id.textViewDaysWeek);
            textViewHour = itemView.findViewById(R.id.textViewHour);
            textViewLocal = itemView.findViewById(R.id.textViewLocal);
            toolbar = itemView.findViewById(R.id.toolbar);
            cardView = itemView.findViewById(R.id.cardView);

            if(!userType.equals("Academia")){
                toolbar.setVisibility(View.INVISIBLE);
            }
        }
    }
}
