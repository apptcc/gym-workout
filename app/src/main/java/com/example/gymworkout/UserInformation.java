package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class UserInformation extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private TextView textViewPesquisar;
    private TextView editTextDocumentMember;
    private String userType;
    private String instructorName;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;
    private CustomAdapterMember customAdapter;
    private List<MemberGym> memberGymList;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BottomNavigationView navigationViewGym;
    private BottomNavigationView navigationViewInstructor;

    private LinearLayout noData;
    private ImageView noDataImage;
    private TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_information);


        floatingActionButton = findViewById(R.id.floatingActionButton);
        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        navigationViewGym = (BottomNavigationView) findViewById(R.id.navigationViewGym);
        navigationViewInstructor = (BottomNavigationView) findViewById(R.id.navigationViewInstructor);

        noData = findViewById(R.id.noData);
        noDataImage = findViewById(R.id.noDataImage);
        noDataText = findViewById(R.id.noDataText);

        menuNavigationGym();
        menuNavigationInstructor();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDialogUserRegistration();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        userType = getIntent().getStringExtra("userType");

        setViewUserType();

        getMemberGymUserType();
    }

    @Override
    public void onRefresh() {
        getMemberGymUserType();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void setViewUserType(){
        if(userType.equals("Professor")){
            getSupportActionBar().setTitle("Lista de Alunos");

            navigationViewGym.setVisibility(View.INVISIBLE);
            floatingActionButton.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Academia")){
            getSupportActionBar().setTitle("Lista de membros");

            navigationViewInstructor.setVisibility(View.INVISIBLE);
        }
    }

    public void menuNavigationGym(){
        navigationViewGym.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationGym: {
                        Intent intent = new Intent(UserInformation.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    } case R.id.navigationProfile: {
                        Intent intent = new Intent(UserInformation.this, Perfil.class);
                        intent.putExtra("userType",userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    } case R.id.navigationUsers: {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void menuNavigationInstructor(){
        navigationViewInstructor.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(UserInformation.this, Workout.class);
                        intent.putExtra("userType",userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        Intent intent = new Intent(UserInformation.this, BodyInformation.class);
                        intent.putExtra("userType",userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(UserInformation.this, GymInformation.class);
                        intent.putExtra("userType",userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    } case R.id.navigationProfile: {
                        Intent intent = new Intent(UserInformation.this, Perfil.class);
                        intent.putExtra("userType",userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    } case R.id.navigationUsers: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public  void displayDialogUserRegistration() {

        Dialog dialog = new Dialog(UserInformation.this);
        dialog.setContentView(R.layout.layout_user_gym_registration);
        setDialogComponents(dialog);

        textViewPesquisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextDocumentMember.getText().toString().isEmpty()) {
                    Toast.makeText(UserInformation.this, "Digite o CPF.", Toast.LENGTH_SHORT).show();
                } else {
                    searchUser();
                }
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setDialogComponents(Dialog dialog){
        editTextDocumentMember = dialog.findViewById(R.id.editTextDocumentMember);
        textViewPesquisar = dialog.findViewById(R.id.textViewPesquisar);
    }

    public void searchUser(){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Users")
        .whereEqualTo("document", editTextDocumentMember.getText().toString())
        .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    if(task.getResult().isEmpty()){
                        Toast.makeText(UserInformation.this, "Nenhum usuário encontrado.", Toast.LENGTH_SHORT).show();
                    } else {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User user = new User();
                            user.setId(document.getString("id"));
                            user.setName(document.getString("name"));
                            user.setType(document.getString("type"));

                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(UserInformation.this);

                            alertDialog.setTitle("Adicionar membro a sua organização?");
                            alertDialog.setMessage("Usuário: " + user.getName() + "\nTipo: " + user.getType());
                            alertDialog.setPositiveButton(Html.fromHtml("<font color='black'>Adicionar</font>"), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    createMemberGym(user);
                                }
                            });
                            alertDialog.setNegativeButton(Html.fromHtml("<font color='black'>Cancelar</font>"), null);
                            alertDialog.setIcon(android.R.drawable.ic_input_add);
                            alertDialog.show();
                        }
                    }
                } else {
                }
            }
        });
    }

    public void createMemberGym(User user){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        MemberGym memberGym = new MemberGym(
                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                user.getId(),
                user.getName(),
                user.getType(),
                simpleDateFormat.format(Calendar.getInstance().getTime())
        );

        db.collection("MemberGym")
        .add(memberGym)
        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(UserInformation.this, "Membro cadastrado com sucesso", Toast.LENGTH_SHORT).show();
            }
        });

        getMemberGymUserType();
    }

    public void getMemberGymUserType(){
        if(userType.equals("Academia")) {
            getMembersGym(FirebaseAuth.getInstance().getCurrentUser().getUid());
        } else {
            FirebaseFirestore db = FirebaseFirestore.getInstance();

            db.collection("MemberGym")
            .whereEqualTo ("userId", FirebaseAuth.getInstance().getCurrentUser().getUid())
            .get()
            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        if(task.getResult().size() >0 ) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                getMembersGym(document.getString("gymId"));
                            }
                        } else {
                            noData.setVisibility(View.VISIBLE);
                            noDataText.setText("Você não está cadastrado em nenhuma academia para visualizar alunos!");
                        }
                    } else {
                    }
                }
            });
        }
    }

    public void getMembersGym(String gymId){
        memberGymList = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("MemberGym")
        .whereEqualTo ("gymId", gymId)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        MemberGym memberGym = new MemberGym();
                        memberGym.setId(document.getId());
                        memberGym.setUserName(document.getString("userName"));
                        memberGym.setUserType(document.getString("userType"));
                        memberGym.setDateCreation(document.getString("dateCreation"));
                        memberGym.setUserId(document.getString("userId"));

                        if(!document.getString("userId").equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                            memberGymList.add(memberGym);
                        } else {
                            instructorName = memberGym.getUserName();
                        }
                    }

                    customAdapter = new CustomAdapterMember(UserInformation.this, memberGymList, userType);
                    recyclerView.setAdapter(customAdapter);

                    if(memberGymList.size() > 0){
                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                        if(!userType.equals("Academia")){
                            noDataText.setText("Sua academia não tem nenhum aluno cadastrado!");
                        }
                    }
                } else {
                }
            }
        });

    }

    public void deleteMemberGym(MemberGym memberGym){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("MemberGym")
        .document(memberGym.getId())
        .delete()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getMemberGymUserType();
    }

    public void profileMemberGym(MemberGym memberGym){
        Intent intent = new Intent(UserInformation.this, Workout.class);
        intent.putExtra("userType","StudentMode");
        intent.putExtra("instructorName", instructorName);
        intent.putExtra("userId", memberGym.getUserId());
        intent.putExtra("userName", memberGym.getUserName());
        startActivity(intent);
    }

}