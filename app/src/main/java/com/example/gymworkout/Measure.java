package com.example.gymworkout;

public class Measure {

    private String id;
    private String userId;
    private String goal;
    private String weight;
    private String desiredWeight;
    private String height;
    private String measureArm;
    private String measureWaist;
    private String measureHip;
    private String measureThigh;
    private String measureDate;

    public Measure(){};

    public Measure(String userId, String goal, String weight, String desiredWeight, String height, String measureArm, String measureWaist, String measureHip, String measureThigh, String measureDate) {
        this.userId = userId;
        this.goal = goal;
        this.weight = weight;
        this.desiredWeight = desiredWeight;
        this.height = height;
        this.measureArm = measureArm;
        this.measureWaist = measureWaist;
        this.measureHip = measureHip;
        this.measureThigh = measureThigh;
        this.measureDate = measureDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDesiredWeight() {
        return desiredWeight;
    }

    public void setDesiredWeight(String desiredWeight) {
        this.desiredWeight = desiredWeight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMeasureArm() {
        return measureArm;
    }

    public void setMeasureArm(String measureArm) {
        this.measureArm = measureArm;
    }

    public String getMeasureWaist() {
        return measureWaist;
    }

    public void setMeasureWaist(String measureWaist) {
        this.measureWaist = measureWaist;
    }

    public String getMeasureHip() {
        return measureHip;
    }

    public void setMeasureHip(String measureHip) {
        this.measureHip = measureHip;
    }

    public String getMeasureThigh() {
        return measureThigh;
    }

    public void setMeasureThigh(String measureThigh) {
        this.measureThigh = measureThigh;
    }

    public String getMeasureDate() {
        return measureDate;
    }

    public void setMeasureDate(String measureDate) {
        this.measureDate = measureDate;
    }
}
