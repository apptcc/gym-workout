package com.example.gymworkout;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class CustomAdapterWorkouts extends RecyclerView.Adapter<CustomAdapterWorkouts.CustomAdapterViewHolder>{

    private Context context;
    private List<Training> trainings;
    private String userType;

    public CustomAdapterWorkouts(Context context, List<Training> trainings, String userType){
        this.context = context;
        this.trainings = trainings;
        this.userType = userType;
    }

    @NonNull
    @Override
    public CustomAdapterWorkouts.CustomAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_training, parent, false);
        return new CustomAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterWorkouts.CustomAdapterViewHolder holder, int position) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Training training = trainings.get(position);

        holder.textViewNameTraining.setText(training.getName());
        holder.textViewDaysWeek.setText(training.getDaysWeek());
        holder.textViewExercisesNumber.setText(training.getExercisesNumber() + " Exercícios");
        holder.textViewInstructor.setText(training.getInstructorName());
        holder.textViewCreationDate.setText(training.getDateCreation());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), WorkoutExercises.class);
                intent.putExtra("id", training.getId());
                intent.putExtra("name", training.getName());
                context.startActivity(intent);
            }
        });

        if(userType.equals("StudentMode") && !training.getInstructorId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            holder.toolbar.setVisibility(View.INVISIBLE);
        }

        holder.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete: {
                        if (context instanceof Workout) {
                            ((Workout)context).deleteTrainingUser(training);
                        }
                        break;
                    }
                    case R.id.update: {
                        if (context instanceof Workout) {
                            ((Workout)context).displayDialogTrainingRegistration(true, training);
                        }
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return trainings.size();
    }


    public class CustomAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewNameTraining;
        public TextView textViewDaysWeek;
        public TextView textViewExercisesNumber;
        public TextView textViewInstructor;
        public TextView textViewCreationDate;
        public CardView cardView;
        public androidx.appcompat.widget.Toolbar toolbar;

        public CustomAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewNameTraining = itemView.findViewById(R.id.textViewNameTraining);
            textViewDaysWeek = itemView.findViewById(R.id.textViewDaysWeek);
            setTextViewDrawableColor(textViewDaysWeek, R.color.colorPrimary);
            textViewExercisesNumber = itemView.findViewById(R.id.textViewExercisesNumber);
            setTextViewDrawableColor(textViewExercisesNumber, R.color.colorPrimary);
            textViewInstructor = itemView.findViewById(R.id.textViewInstructor);
            textViewCreationDate = itemView.findViewById(R.id.textViewCreationDate);
            toolbar = itemView.findViewById(R.id.toolbar);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
}
