package com.example.gymworkout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

public class CustomAdapterMeasure extends RecyclerView.Adapter<CustomAdapterMeasure.CustomAdapterViewHolder>{

    private Context context;
    private List<Measure> measures;
    private String userType;

    public CustomAdapterMeasure(Context context, List<Measure> measures, String userType){
        this.context = context;
        this.measures = measures;
        this.userType = userType;
    }

    @NonNull
    @Override
    public CustomAdapterMeasure.CustomAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_view_measure, parent, false);
        return new CustomAdapterMeasure.CustomAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterMeasure.CustomAdapterViewHolder holder, int position) {

        Measure measure = measures.get(position);

        holder.textViewGoal.setText(measure.getGoal());
        holder.textViewWeight.setText(measure.getWeight() + " kg");
        holder.textViewDesiredWeight.setText(measure.getDesiredWeight() + " kg");
        holder.textViewHeight.setText(measure.getHeight() + " cm");
        holder.textViewMeasureArm.setText(measure.getMeasureArm() + " cm");
        holder.textViewMeasureWaist.setText(measure.getMeasureWaist() + " cm");
        holder.textViewMeasureHip.setText(measure.getMeasureHip() + " cm");
        holder.textViewMeasureThigh.setText(measure.getMeasureThigh() + " cm");
        holder.textViewMeasureDate.setText(measure.getMeasureDate());

        holder.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.delete: {
                        if (context instanceof BodyInformation) {
                            ((BodyInformation)context).deleteMeasureUser(measure);
                        }
                        break;
                    }
                    case R.id.update: {
                        if (context instanceof BodyInformation) {
                            ((BodyInformation)context).displayBottomSheetMeasureRegistration(true, measure);
                        }
                        break;
                    }
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return measures.size();
    }

    public class CustomAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewGoal;
        public TextView textViewWeight;
        public TextView textViewDesiredWeight;
        public TextView textViewHeight;
        public TextView textViewMeasureArm;
        public TextView textViewMeasureWaist;
        public TextView textViewMeasureHip;
        public TextView textViewMeasureThigh;
        public TextView textViewMeasureDate;
        public CardView cardView;
        public androidx.appcompat.widget.Toolbar toolbar;

        public CustomAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewGoal = itemView.findViewById(R.id.textViewGoal);
            textViewWeight = itemView.findViewById(R.id.textViewWeight);
            textViewDesiredWeight = itemView.findViewById(R.id.textViewDesiredWeight);
            textViewHeight = itemView.findViewById(R.id.textViewHeight);
            textViewMeasureArm = itemView.findViewById(R.id.textViewMeasureArm);
            textViewMeasureWaist = itemView.findViewById(R.id.textViewMeasureWaist);
            textViewMeasureHip = itemView.findViewById(R.id.textViewMeasureHip);
            textViewMeasureThigh = itemView.findViewById(R.id.textViewMeasureThigh);
            textViewMeasureDate = itemView.findViewById(R.id.textViewMeasureDate);
            toolbar = itemView.findViewById(R.id.toolbar);
            cardView = itemView.findViewById(R.id.cardView);

            if(userType.equals("StudentMode")){
                toolbar.setVisibility(View.INVISIBLE);
            }
        }
    }
}
