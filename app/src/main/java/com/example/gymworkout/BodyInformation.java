package com.example.gymworkout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BodyInformation extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private String userType;

    private EditText editTextGoal;
    private EditText editTextWeight;
    private EditText editTextDesiredWeight;
    private EditText editTextHeight;
    private EditText editTextMeasureArm;
    private EditText editTextMeasureWaist;
    private EditText editTextMeasureHip;
    private EditText editTextMeasureThigh;
    private TextView textViewCadastrar;

    private List<Measure> measures;

    private RecyclerView recyclerView;

    private CustomAdapterMeasure customAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private BottomNavigationView navigationView;
    private BottomNavigationView navigationViewInstructor;
    private BottomNavigationView navigationViewStudentMode;
    private FloatingActionButton floatingActionButton;

    private LinearLayout noData;
    private ImageView noDataImage;
    private TextView noDataText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_body_information);

        getSupportActionBar().setTitle("Medidas Corporais");


        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationViewInstructor = (BottomNavigationView) findViewById(R.id.navigationViewInstructor);
        navigationViewStudentMode = (BottomNavigationView) findViewById(R.id.navigationViewStudentMode);

        floatingActionButton = findViewById(R.id.floatingActionButton);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        noData = findViewById(R.id.noData);
        noDataImage = findViewById(R.id.noDataImage);
        noDataText = findViewById(R.id.noDataText);

        recyclerView = findViewById(R.id.recyclerView);

        menuNavigation();
        menuNavigationInstructor();
        menuNavigationStudentMode();

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayBottomSheetMeasureRegistration(false, null);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        userType = getIntent().getStringExtra("userType");

        setViewUserType();

        getMeasuresUserType();
    }

    private void setViewUserType(){
        if(userType.equals("Aluno")) {
            navigationView.setSelectedItemId(R.id.navigationMeasure);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
            navigationViewStudentMode.setVisibility(View.INVISIBLE);
        } else if(userType.equals("Professor")){
            navigationViewInstructor.setSelectedItemId(R.id.navigationMeasure);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewStudentMode.setVisibility(View.INVISIBLE);
        } else if(userType.equals("StudentMode")){
            getSupportActionBar().setTitle("Medidas Corporais " + getIntent().getStringExtra("userName"));
            navigationViewStudentMode.setSelectedItemId(R.id.navigationMeasure);
            navigationView.setVisibility(View.INVISIBLE);
            navigationViewInstructor.setVisibility(View.INVISIBLE);
            floatingActionButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        getMeasuresUserType();
        swipeRefreshLayout.setRefreshing(false);
    }

    public void menuNavigation(){
        navigationView.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(BodyInformation.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(BodyInformation.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(BodyInformation.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationInstructor(){
        navigationViewInstructor.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationUsers: {
                        Intent intent = new Intent(BodyInformation.this, UserInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(BodyInformation.this, Workout.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationGym: {
                        Intent intent = new Intent(BodyInformation.this, GymInformation.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationProfile: {
                        Intent intent = new Intent(BodyInformation.this, Perfil.class);
                        intent.putExtra("userType", userType);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void menuNavigationStudentMode(){
        navigationViewStudentMode.setOnItemSelectedListener(new BottomNavigationView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigationWorkout: {
                        Intent intent = new Intent(BodyInformation.this, Workout.class);
                        intent.putExtra("userType", userType);
                        intent.putExtra("instructorName", getIntent().getStringExtra("instructorName"));
                        intent.putExtra("userId", getIntent().getStringExtra("userId"));
                        intent.putExtra("userName", getIntent().getStringExtra("userName"));
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    }
                    case R.id.navigationExit: {
                        Intent intent = new Intent(BodyInformation.this, UserInformation.class);
                        intent.putExtra("userType", "Professor");
                        startActivity(intent);
                        return true;
                    }
                    case R.id.navigationMeasure: {
                        return true;
                    }
                }

                return false;
            }
        });
    }

    public void displayBottomSheetMeasureRegistration(Boolean update, Measure measure){
        Dialog dialog = new Dialog(BodyInformation.this);
        dialog.setContentView(R.layout.layout_measure_registration);
        setDialogComponents(dialog);

        if(update){
            textViewCadastrar.setText("Atualizar");
            editTextGoal.setText(measure.getGoal());
            editTextWeight.setText(measure.getWeight());
            editTextDesiredWeight.setText(measure.getDesiredWeight());
            editTextHeight.setText(measure.getHeight());
            editTextMeasureArm.setText(measure.getMeasureArm());
            editTextMeasureWaist.setText(measure.getMeasureWaist());
            editTextMeasureHip.setText(measure.getMeasureHip());
            editTextMeasureThigh.setText(measure.getMeasureThigh());

            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateMeasureUser(measure);
                    dialog.dismiss();
                }
            });

        } else {
            textViewCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createMeasureUser();
                    dialog.dismiss();
                }
            });
        }

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setDialogComponents(Dialog dialog){
        editTextGoal = dialog.findViewById(R.id.editTextGoal);
        editTextWeight = dialog.findViewById(R.id.editTextWeight);
        editTextDesiredWeight = dialog.findViewById(R.id.editTextDesiredWeight);
        editTextHeight = dialog.findViewById(R.id.editTextHeight);
        editTextMeasureArm = dialog.findViewById(R.id.editTextMeasureArm);
        editTextMeasureWaist = dialog.findViewById(R.id.editTextMeasureWaist);
        editTextMeasureHip = dialog.findViewById(R.id.editTextMeasureHip);
        editTextMeasureThigh = dialog.findViewById(R.id.editTextMeasureThigh);
        textViewCadastrar = dialog.findViewById(R.id.textViewCadastrar);
    }

    public void getMeasuresUserType(){
        if(userType.equals("StudentMode")){
            getMeasures(getIntent().getStringExtra("userId"));
        } else {
            getMeasures(FirebaseAuth.getInstance().getCurrentUser().getUid());
        }
    }

    public void getMeasures(String userId){
        measures = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Measure")
        .whereEqualTo ("userId", userId)
        .get()
        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Measure measure = new Measure();

                        measure.setId(document.getId());
                        measure.setDesiredWeight(document.getString("desiredWeight"));
                        measure.setGoal(document.getString("goal"));
                        measure.setHeight(document.getString("height"));
                        measure.setUserId(document.getString("userId"));
                        measure.setMeasureArm(document.getString("measureArm"));
                        measure.setMeasureHip(document.getString("measureHip"));
                        measure.setMeasureThigh(document.getString("measureThigh"));
                        measure.setMeasureWaist(document.getString("measureWaist"));
                        measure.setWeight(document.getString("weight"));
                        measure.setMeasureDate(document.getString("measureDate"));

                        measures.add(measure);
                    }

                    customAdapter = new CustomAdapterMeasure(BodyInformation.this, measures, userType);
                    recyclerView.setAdapter(customAdapter);

                    if(measures.size() > 0){
                        noData.setVisibility(View.GONE);
                    } else {
                        noData.setVisibility(View.VISIBLE);
                    }
                } else {
                }
            }
        });
    }

    public void createMeasureUser(){

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Measure measure = new Measure(
                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                editTextGoal.getText().toString(),
                editTextWeight.getText().toString(),
                editTextDesiredWeight.getText().toString(),
                editTextHeight.getText().toString(),
                editTextMeasureArm.getText().toString(),
                editTextMeasureWaist.getText().toString(),
                editTextMeasureHip.getText().toString(),
                editTextMeasureThigh.getText().toString(),
                simpleDateFormat.format(Calendar.getInstance().getTime())
        );

        db.collection("Measure")
        .add(measure)
        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
                Toast.makeText(BodyInformation.this, "Medidas cadastradas com sucesso", Toast.LENGTH_SHORT).show();
            }
        });

        getMeasuresUserType();
    }

    public void updateMeasureUser(Measure measure){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Measure")
        .document(measure.getId())
        .update( "goal", editTextGoal.getText().toString(),
                "weight", editTextWeight.getText().toString(),
                "desiredWeight", editTextDesiredWeight.getText().toString(),
                "height", editTextHeight.getText().toString(),
                "measureArm", editTextMeasureArm.getText().toString(),
                "measureWaist", editTextMeasureWaist.getText().toString(),
                "measureHip", editTextMeasureHip.getText().toString(),
                "measureThigh", editTextMeasureThigh.getText().toString()
                )
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                Toast.makeText(BodyInformation.this, "Medidas atualizadas com sucesso", Toast.LENGTH_SHORT).show();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getMeasuresUserType();
    }

    public void deleteMeasureUser(Measure measure){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("Measure").document(measure.getId())
        .delete()
        .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(BodyInformation.this, "Medidas excluídas com sucesso", Toast.LENGTH_SHORT).show();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

        getMeasuresUserType();
    }
}